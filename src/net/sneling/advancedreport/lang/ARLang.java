package net.sneling.advancedreport.lang;

import net.sneling.advancedreport.AdvancedReport;
import net.sneling.snelapi.language.SnelLanguage;
import net.sneling.snelapi.language.SnelLanguageParent;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ARLang extends SnelLanguageParent {
    
    public ARLang(){
        super("advancedreport", "lang", AdvancedReport.getInstance());
    }
    
    public static SnelLanguage
        COMMAND_DESCRIPTION = new SnelLanguage("Main Command for AdvancedReport", false),
        COMMAND_LISTMODS_DESCRIPTION = new SnelLanguage("See how many Report Moderators are online", false),

        COMMAND_LISTMODS_MESSAGE = new SnelLanguage("&7There are &a%AMOUNT% &7Report Moderators online."),

        COMMAND_REPORT_SELF = new SnelLanguage("&cYou cannot report yourself.. Please login with another account to report yourself."),

        REPORT_INFORM_REPORTED = new SnelLanguage("&cYou have been reported for %REASON%."),
        REPORT_INFORM_REPORTER = new SnelLanguage("&aYou reported %REPORTED% for %REASON%."),
        REPORT_INFORM_MODLIST = new SnelLanguage("&c%REPORTED%&a was reported by %REPORTER% for %REASON%. &6[%ID%]"),
        REPORT_ERROR = new SnelLanguage("&cAn error occured while trying to log this report. Please contact an admin.");

}
