package net.sneling.advancedreport.managers.report;

import net.sneling.snelapi.util.identifiable.IDUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Report {

    private Player reporter;
    private OfflinePlayer reported;
    private String reason, id;
    private Date date;

    public Report(Player reporter, OfflinePlayer reported, String reason) {
        this.reporter = reporter;
        this.reported = reported;
        this.reason = reason;
        this.id = reported.getName().toLowerCase() + "-" + IDUtil.generateRandomID(3);
        this.date = Calendar.getInstance().getTime();
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public Player getReporter() {
        return reporter;
    }

    public OfflinePlayer getReported() {
        return reported;
    }

    public String getReason() {
        return reason;
    }
}
