package net.sneling.advancedreport.managers.report;

import net.sneling.advancedreport.AdvancedReport;
import net.sneling.advancedreport.lang.ARLang;
import net.sneling.advancedreport.util.Mod;
import net.sneling.snelapi.logger.Logger;
import org.bukkit.entity.Player;

import java.io.IOException;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Reporter {

    public static void report(Report report){
        Logger.debug("Filing report from " + report.getReporter() + " for " + report.getReported() + ", for " + report.getReason() + " with id " + report.getId(), AdvancedReport.getInstance());

        try {
            AdvancedReport.getInstance().getReportLogger().logReport(report);
        } catch (IOException e) {
            report.getReporter().sendMessage(ARLang.REPORT_ERROR.get());
            return;
        }

        Mod.sendMessage(replace(ARLang.REPORT_INFORM_MODLIST.get(), report));

        if(report.getReported().isOnline())
            ((Player) report.getReported()).sendMessage(replace(ARLang.REPORT_INFORM_REPORTED.get(), report));

        report.getReporter().sendMessage(replace(ARLang.REPORT_INFORM_REPORTER.get(), report));
        Logger.debug("Filed report ", AdvancedReport.getInstance());
    }

    private static String replace(String s, Report r){
        return s.replaceAll("%REPORTER%", r.getReporter().getName()).replaceAll("%REPORTED%", r.getReported().getName()).replaceAll("%REASON%", r.getReason()).replaceAll("%ID%", r.getId());
    }

}
