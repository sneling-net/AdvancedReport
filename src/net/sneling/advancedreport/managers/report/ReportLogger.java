package net.sneling.advancedreport.managers.report;

import net.sneling.advancedreport.AdvancedReport;
import net.sneling.snelapi.file.text.SnelTextFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by Sneling on 9/13/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ReportLogger extends SnelTextFile {

    public ReportLogger() throws IOException {
        super(AdvancedReport.getInstance(), new File(AdvancedReport.getInstance().getDataFolder() + "/reports.log"));

        if(!getFile().exists()) {
            createFile();

            openWriter(false);
            addLine("[ID @ DATE] Reporter > Reported : Reason");
            closeWriter();
        }
    }

    void logReport(Report report) throws IOException {
        String message =
                "[" + report.getId() + " @ " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(report.getDate()) + "] "
                        + report.getReporter().getName() + " (" + report.getReporter().getUniqueId().toString() + ")"
                        + " > "
                        + report.getReported().getName() + "(" + report.getReported().getUniqueId().toString() + ") : " + report.getReason();

        openWriter(false);
        addLine(message);
        closeWriter();
    }

}
