package net.sneling.advancedreport.commands;

import net.sneling.advancedreport.lang.ARLang;
import net.sneling.advancedreport.permissions.ARPerm;
import net.sneling.advancedreport.util.Mod;
import net.sneling.snelapi.commands.SnelCommand;
import org.bukkit.command.CommandSender;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
class ListModsCommand extends SnelCommand {

    ListModsCommand() {
        super("listmods");

        setPermission(ARPerm.COMMAND_LISTMODS);
        setDescription(ARLang.COMMAND_LISTMODS_DESCRIPTION.get());
    }

    public void execute(CommandSender sender, String[] args){
        sender.sendMessage(ARLang.COMMAND_LISTMODS_MESSAGE.get().replaceAll("%AMOUNT%", Mod.count() + ""));
    }

}
