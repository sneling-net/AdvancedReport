package net.sneling.advancedreport.commands;

import net.sneling.advancedreport.lang.ARLang;
import net.sneling.advancedreport.managers.report.Report;
import net.sneling.advancedreport.managers.report.Reporter;
import net.sneling.advancedreport.permissions.ARPerm;
import net.sneling.snelapi.commands.SnelCommand;
import net.sneling.snelapi.commands.arg.ArgInfo;
import net.sneling.snelapi.commands.arg.ArgRequirement;
import net.sneling.snelapi.commands.arg.ArgType;
import net.sneling.snelapi.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class AdvancedReportCommand extends SnelCommand {

    private ListModsCommand listModsCommand;

    public AdvancedReportCommand() {
        super("report");

        setDescription(ARLang.COMMAND_DESCRIPTION.get());
        setPermission(ARPerm.COMMAND);

        addChild(listModsCommand = new ListModsCommand());

        setForcePlayerExecuter(true);
        addArgument(new ArgInfo("player", ArgType.MENDATORY, ArgRequirement.PLAYER));
        addArgument(new ArgInfo("reason", ArgType.MENDATORY));
    }

    protected void execute(CommandSender sender, String[] args){
        Player reporter = (Player) sender;
        OfflinePlayer reported = Bukkit.getOfflinePlayer(args[0]);
        String reason = StringUtil.toSentence(args, 1);

        if(reporter.getUniqueId().equals(reported.getUniqueId())){
            reporter.sendMessage(ARLang.COMMAND_REPORT_SELF.get());
            return;
        }

        Report report = new Report(reporter, reported, reason);

        Reporter.report(report);
    }


}
