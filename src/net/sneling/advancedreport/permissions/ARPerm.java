package net.sneling.advancedreport.permissions;

import net.sneling.advancedreport.AdvancedReport;
import net.sneling.snelapi.permission.SnelPermission;
import net.sneling.snelapi.permission.SnelPermissionParent;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ARPerm extends SnelPermissionParent {

    public static SnelPermission
        COMMAND,
        COMMAND_LISTMODS,

        MOD,
        MOD_LISTED,
        MOD_SEE;


    public ARPerm() {
        super("advancedreport", AdvancedReport.getInstance());
    }

}
