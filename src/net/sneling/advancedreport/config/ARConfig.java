package net.sneling.advancedreport.config;

import net.sneling.advancedreport.AdvancedReport;
import net.sneling.snelapi.file.yml.SnelYMLConfig;

/**
 * Created by Sneling on 9/14/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ARConfig extends SnelYMLConfig {

    public ARConfig() {
        super(AdvancedReport.getInstance(), "config", true);
    }



}
