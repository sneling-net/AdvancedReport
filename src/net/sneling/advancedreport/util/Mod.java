package net.sneling.advancedreport.util;

import net.sneling.advancedreport.permissions.ARPerm;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Mod {

    public static int count(){
        int count = 0;
        for(Player p: Bukkit.getOnlinePlayers())
            if(ARPerm.MOD_LISTED.has(p))
                count++;

        return count;
    }

    public static void sendMessage(String msg){
        for(Player p: Bukkit.getOnlinePlayers())
            if(ARPerm.MOD_SEE.has(p))
                p.sendMessage(msg);
    }

}
