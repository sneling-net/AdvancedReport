package net.sneling.advancedreport;


import net.sneling.advancedreport.commands.AdvancedReportCommand;
import net.sneling.advancedreport.config.ARConfig;
import net.sneling.advancedreport.lang.ARLang;
import net.sneling.advancedreport.managers.report.ReportLogger;
import net.sneling.advancedreport.permissions.ARPerm;
import net.sneling.snelapi.plugin.SnelPlugin;

import java.io.IOException;

/**
 * Created by Sneling on 9/12/2016 for AdvancedReport.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class AdvancedReport extends SnelPlugin{

    private static AdvancedReport instance;

    private AdvancedReportCommand advancedReportCommand;

    private ReportLogger reportLogger;

    private ARConfig config;

    private ARLang arLang;

    private ARPerm arPerm;

    public AdvancedReport() {
        super("AdvancedReport", "A-Rep", "1.0A", "sneling-advancedreport");

        instance = this;

        if(!getDataFolder().exists())
            getDataFolder().mkdirs();

        register();

        config = new ARConfig();

        try {
            reportLogger = new ReportLogger();
        } catch (IOException e) {
            e.printStackTrace();
        }

        arLang = new ARLang();
    }

    public void onPluginEnable(){
        arPerm = new ARPerm();

        registerCommand(advancedReportCommand = new AdvancedReportCommand());
    }

    public static AdvancedReport getInstance() {
        return instance;
    }

    public AdvancedReportCommand getAdvancedReportCommand() {
        return advancedReportCommand;
    }

    public ReportLogger getReportLogger() {
        return reportLogger;
    }
}
